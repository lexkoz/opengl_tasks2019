add_definitions(-DGLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
    main.cpp
    BreatherSurface.cpp
    common/Application.cpp
    common/Camera.cpp
    common/DebugOutput.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp  
)

set(HEADER_FILES
    BreatherSurface.hpp
    common/Application.hpp
    common/Camera.cpp
    common/DebugOutput.h
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

include_directories(common)

MAKE_OPENGL_TASK(592Tverdov 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(592Tverdov1 stdc++fs)
endif()
