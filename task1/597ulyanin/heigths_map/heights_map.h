#pragma once

#include <string_view>
#include <SOIL2.h>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>


class HeightsMap {
public:
    HeightsMap() = default;


    virtual ~HeightsMap() = default;


    HeightsMap(const unsigned char* data, int width, int height) {
        LoadFromData(data, width, height);
    }


    HeightsMap(std::string_view path) {
        LoadFromFile(path);
    }


    void LoadFromFile(std::string_view path) {
        assert(std::filesystem::exists(path));

        int width, height, channels;
        unsigned char* data = SOIL_load_image(path.data(), &width, &height, &channels, SOIL_LOAD_L);

        if (data == nullptr) {
            throw std::runtime_error(SOIL_last_result());
        }

        if (channels != 1) {
            SOIL_free_image_data(data);
            throw std::runtime_error(std::string("expected 1 channel for ") + std::string(path) + ", found: " + std::to_string(channels) + "; use gimp to convert");
        }

        LoadFromData(data, width, height);
        SOIL_free_image_data(data);
    }


    void LoadFromData(const unsigned char* data, int width, int height) {
        Heights.resize(height);
        for (int i = 0; i < height; ++i, data += width) {
            Heights[i].resize(width);
            for (int j = 0; j < width; ++j) {
                Heights[i][j] = static_cast<float>(data[j]) / 255;
            }
        }
    }


    size_t GetWidth() const {
        return Heights.empty() ? 0 : Heights[0].size();
    }


    size_t GetHeight() const {
        return Heights.size();
    }


    std::pair<size_t, size_t> GetWidthHeight() const {
        if (Heights.empty()) {
            return {0, 0};
        } else {
            return {Heights.size(), Heights[0].size()};
        }
    }


    std::vector<std::vector<float>> Heights;
};