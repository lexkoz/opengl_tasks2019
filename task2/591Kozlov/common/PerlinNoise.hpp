#pragma once

#include <vector>
#include <random>
#include <cmath>
#include <fstream>

class PerlinNoise {
private:
	struct Vec {
		float x;
		float y;
	};

public:
	PerlinNoise() = default;

	void GenerateBuffer(size_t size = 1024, int seed = 42) {
	    std::vector<int> newBuf(size);
        srand(seed);
        for (size_t i = 0; i < newBuf.size(); i++) {
            newBuf[i] = rand();
        }
        buffer_.swap(newBuf);
	}

	void SaveBuffer(const std::string fname) const {
	    std::fstream stream(fname, std::fstream::out);
	    for (const auto& num : buffer_) {
	        stream << num << " ";
	    }
	}

	void LoadBuffer(const std::string fname) {
	    std::fstream stream(fname, std::fstream::in);
	    int num = 0;
	    std::vector<int> newBuf;
	    while(!stream.eof()) {
	        stream >> num;
	        newBuf.push_back(num);
	    }
	    buffer_.swap(newBuf);
	}

	float Noise(float x, float y, int oct=5, float persistence = 0.5f) {
		float amplitude = 1.0f;

		float max = 0;
		float result = 0;

		while (oct-- > 0) {
			max += amplitude;
			result += DoNoise(x, y) * amplitude;
			amplitude *= persistence;
			x *= 2;
			y *= 2;
		}
		return result/max;
	}

	float DoNoise(float x, float y) const {
		int left = floor(x);
		int top = floor(y);

		float dist_x = x - left;
		float dist_y = y - top;

		Vec topLeftGrad = GetRandomGrad(left, top);
		Vec topRightGrad = GetRandomGrad(left + 1, top);
		Vec bottomLeftGrad = GetRandomGrad(left, top + 1);
		Vec bottomRightGrad = GetRandomGrad(left + 1, top + 1);

		Vec topLeftDist{dist_x, dist_y};
		Vec topRightDist{dist_x - 1, dist_y};
		Vec bottomLeftDist{dist_x, dist_y - 1};
		Vec bottomRightDist{dist_x - 1, dist_y - 1};
		
		float d1 = Dot(topLeftDist, topLeftGrad);
		float d2 = Dot(topRightDist, topRightGrad);
		float d3 = Dot(bottomLeftDist, bottomLeftGrad);
		float d4 = Dot(bottomRightDist, bottomRightGrad);

		float topInterpol = Interpol(d1, d2, dist_x);
		float bottomInterpol = Interpol(d3, d4, dist_x);
		return Interpol(topInterpol, bottomInterpol, dist_y);
	}

private:
	float QunticCurve(float t) const {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}

	float Interpol(float a, float b, float t) const {
		return a + (b - a) * QunticCurve(t);
	}

	float Dot(const Vec& a, const Vec& b) const {
		return a.x * b.x + a.y * b.y;
	}

	Vec GetRandomGrad(float x, float y) const {
		int v = (int)(((int)pow(x * 1836311903, y * 2971215073) + 4807526976) & 1023);
		switch (buffer_[v] % 3) {
			case 0: return Vec{1.f, 0.f};
			case 1: return Vec{-1.f, 0.f};
			case 2: return Vec{0.f, 1.f};
			default : return Vec{0.f, -1.f};

		}
	}

private:
	std::vector<int> buffer_;
};
