#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/Camera.hpp"
#include "common/ShaderProgram.hpp"
#include "common/PerlinNoise.hpp"
#include "common/Terrain.hpp"
#include "common/LightInfo.hpp"
#include "common/Texture.hpp"

#include <vector>

using PointData = std::vector<std::pair<int, glm::vec3>>;
using DataNormals = std::vector<std::vector<PointData>>;

TerrainPtr BuildTerrain(size_t freq, float size) {
	std::vector<glm::vec3> verts;
	DataNormals point2normals(freq + 1, std::vector<PointData>(freq + 1, PointData()));

	PerlinNoise pn;
	pn.GenerateBuffer();

	float step = 2.0f / freq;
    float shakalnost = 5.f;
	size_t index_x = 0;
	size_t index_y = 0;
	size_t num = 0;
    std::vector<glm::vec2> tex_coords;
    std::vector<glm::vec2> map_coords;
	for (float i = -1.0f, index_x = 0; i < 1.0f; i+=step, ++index_x) {
		for (float j = -1.0f, index_y = 0; j < 1.0f; j+=step, ++index_y) {
			glm::vec3 v1(i * size, j * size, pn.Noise(i, j));
			glm::vec3 v2((i + step) * size, j * size, pn.Noise(i + step, j));
			glm::vec3 v3(i * size, (j + step) * size, pn.Noise(i, j + step));
			glm::vec3 v4((i + step) * size, (j + step) * size, pn.Noise(i + step, j + step));

			verts.push_back(v1);
			verts.push_back(v2);
			verts.push_back(v3);
			verts.push_back(v2);
			verts.push_back(v3);
			verts.push_back(v4);

			glm::vec3 norm1 = glm::normalize(glm::cross(v3 - v1, v2 - v1));
			glm::vec3 norm2 = glm::normalize(glm::cross(v3 - v2, v4 - v2));

			point2normals[index_x][index_y].push_back(std::make_pair(num++, norm1));
			point2normals[index_x + 1][index_y].push_back(std::make_pair(num++, norm1));
			point2normals[index_x][index_y + 1].push_back(std::make_pair(num++, norm1));
			point2normals[index_x + 1][index_y].push_back(std::make_pair(num++, norm2));
			point2normals[index_x][index_y + 1].push_back(std::make_pair(num++, norm2));
			point2normals[index_x + 1][index_y + 1].push_back(std::make_pair(num++, norm2));

            float tx = (i + 1.f) * shakalnost;
            float ty = (j + 1.f) * shakalnost;
            tex_coords.push_back(glm::vec2(tx, ty));
            tex_coords.push_back(glm::vec2(tx + step * shakalnost, ty));
            tex_coords.push_back(glm::vec2(tx, ty + step * shakalnost));

            float x = (i + 1.f) / 2.f;
            float y = (j + 1.f) / 2.f;
            map_coords.push_back(glm::vec2(x, y));
            map_coords.push_back(glm::vec2(x + step / 2.f, y));
            map_coords.push_back(glm::vec2(x, y + step / 2.f));

            tex_coords.push_back(glm::vec2(tx, ty + step * shakalnost));
            tex_coords.push_back(glm::vec2(tx + step * shakalnost, ty + step * shakalnost));
            tex_coords.push_back(glm::vec2(tx + step * shakalnost, ty));
            map_coords.push_back(glm::vec2(x, y + step / 2.f));
            map_coords.push_back(glm::vec2(x + step / 2.f, y + step / 2.f));
            map_coords.push_back(glm::vec2(x + step / 2.f, y));
		}
	}

	std::vector<glm::vec3> normals(num);
	for (int i = 0; i < point2normals.size(); i++) {
		for (int j = 0; j < point2normals.size(); j++) {
			glm::vec3 res_norm;
			for (const auto& norm : point2normals[i][j]) {
				res_norm += norm.second;
			}
			res_norm /= point2normals[i][j].size();
			for (const auto& norm : point2normals[i][j]) {
				normals[norm.first] = res_norm;
			}
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(verts.size() * sizeof(float) * 3, verts.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(tex_coords.size() * sizeof(float) * 2, tex_coords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(map_coords.size() * sizeof(float) * 2, map_coords.data());

	TerrainPtr mesh = std::make_shared<Terrain>(size, freq, pn);
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount((GLuint)verts.size());
	return mesh;
}


class TerrainApplication : public Application {
public:
	void makeScene() override {
		Application::makeScene();

		_mesh = BuildTerrain(700U, 3.0f);
		_cameraMover = std::make_shared<FirstPersonCameraMover>(_mesh);

		_mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		_shader = std::make_shared<ShaderProgram>("591KozlovData2/shader.vert", "591KozlovData2/shader.frag");

		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(0.2, 0.2, 0.2);

		_grass = loadTexture("591KozlovData2/grass.jpg");
		_sand = loadTexture("591KozlovData2/sand.jpg");
		_snow = loadTexture("591KozlovData2/snow.jpg");
		//_stone = loadTexture("591KozlovData2/stone.jpg");
		_map = loadTexture("591KozlovData2/map.jpg");
		_specular = loadTexture("591KozlovData2/specular.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
		}
		ImGui::End();
	}


	void draw() override {
		Application::draw();

		int width, height;

		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mesh->modelMatrix()))));

        glm::vec3 lightDir = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
        _shader->setVec3Uniform("lightDir", _lightDir);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _snow->bind();
        _shader->setIntUniform("snowTex", 0);

        glActiveTexture(GL_TEXTURE0 + 1);
        _grass->bind();
        _shader->setIntUniform("grassTex", 1);

        glActiveTexture(GL_TEXTURE0 + 2);
        _sand->bind();
        _shader->setIntUniform("sandTex", 2);

        glActiveTexture(GL_TEXTURE0 + 3);
        _map->bind();
        _shader->setIntUniform("mapTex", 3);

        glActiveTexture(GL_TEXTURE0 + 4);
        _specular->bind();
        _shader->setIntUniform("specularTex", 4);

        _mesh->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
	}

private:
	TerrainPtr _mesh;
	ShaderProgramPtr _shader;
	glm::vec3 _lightDir;
	float _phi = 0.0f;
	float _theta = glm::pi<float>() * 0.25f;
	TexturePtr _grass, _snow, _sand, _map, _specular;
	GLuint _sampler;
	glm::vec3 _lightAmbientColor;
	glm::vec3 _lightDiffuseColor;
	LightInfo _light;
};


int main() {
	TerrainApplication app;
	app.start();
	
	return 0;
}
