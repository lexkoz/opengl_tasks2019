#pragma once

#include "Mesh.hpp"
#include "PerlinNoise.hpp"

class Terrain;

using TerrainPtr = std::shared_ptr<Terrain>;

class Terrain : public Mesh {
public:
	Terrain(float size, size_t freq, const PerlinNoise& pn)
		: size_(size)
		, freq_(freq)
		, pn_(pn)
	{
	}

	float GetHeight(float x, float y) {
		return pn_.Noise(x / size_, y / size_);
	}

private:
	float size_;
	size_t freq_;
	PerlinNoise pn_;

};
